# IP Fabric Webhook Integration for Microsoft Teams

**EXPERIMENTAL NOT MEANT FOR PRODUCTION**

This will send an alert to MS Teams when certain Snapshot or Intent Rules are processed in IP Fabric.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Installing

Log into IP Fabric as osadmin:

```bash
sudo su
wget https://gitlab.com/ip-fabric/integrations/scripts/ipfabric-teams-webhook/-/raw/main/install.sh -O install.sh
chmod +x install.sh
./install.sh
```

Edit `/home/integrations/ipfabric-teams-webhook/.env` and optionally `/home/integrations/ipfabric-teams-webhook/ipf_alerts.json`

Then run install script again: `/home/integrations/ipfabric-teams-webhook/install.sh` as root.

Verifying service: `systemctl status ipf-teams-webhook.service`

## Setup

### IP Fabric Setup

- Go to Settings > Webhooks > Add webhook
- Provide a name
- URL will be: `http(s)://<YOUR IP/DNS>:8000/ipfabric`
- Copy secret
- Select if you want both Snapshot and Intent Events
  - Recommended for only Snapshot events.

### Environment Setup

Copy `sample.env` to `.env` and edit the file (`cp sample.env .env`):

- `IPF_SECRET` is found in the webhook settings page
- `IPF_TOKEN` is an API token created in Settings > API Token
  - Token must have access to the following endpoints
    - GET `/users` (Optional but Teams will not be able to translate userId to username without this.)
    - GET `/snapshots`
    - POST `/tables/management/snapshots`
- `IPF_TEST` will not send test alerts to the channel when set to `false`
- `TEAMS_URL` is found when adding an "Incoming Webhook" on a Teams Channel

If running on IP Fabric VM then the following can be left as default.

- `IPF_URL` must be like `https://127.0.0.1/` without any trailing information.
- `IPF_VERIFY=false` if your IP Fabric SSL cert is not trusted or running via 127.0.0.1

### Alerting Config

Default IP Fabric alerts can be found in [sample_ipf_alerts.json](sample_ipf_alerts.json).
This is copied to `ipf_alerts.json` during install which can be edited to turn on or off which alerts you care about.

## Manually Running

```shell
su - integrations
cd ipfabric-teams-webhook
poetry run uvicorn api:app --host 127.0.0.1 --port 8000 --app-dir /home/integrations/ipfabric-teams-webhook/ipfabric-teams-webhook
```
