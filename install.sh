#!/bin/bash

echo "Installing ipfabric-teams-webhook"

if [ -f /home/integrations/ipfabric-teams-webhook/test_connection.py ]; then
    echo "Project installed testing connection."
    echo
    sudo runuser - integrations -c "/home/integrations/ipfabric-teams-webhook/test_connection.py"
    echo

    if [ $? -eq 0 ]; then
      echo "Environment Successful"

      if [ -f /etc/systemd/system/ipf-teams-webhook.service ]; then
        echo "Testing service."
        systemctl is-active ipf-teams-webhook.service --quiet
        STATUS=$?

        if [ $STATUS -eq 0 ]; then
          echo
          echo "Project installed and running."
          exit 0
        else
          echo
          echo "Starting service."
          systemctl start ipf-teams-webhook.service
          sleep 5
          echo "Printing service status."
          systemctl show -p ActiveState ipf-teams-webhook
          exit 0
        fi

      else
        echo
        echo "Copying System Service."
        cp /home/integrations/ipfabric-teams-webhook/ipf-teams-webhook.service /etc/systemd/system/ipf-teams-webhook.service
        chown root:root /etc/systemd/system/ipf-teams-webhook.service
        echo "Reloading and enabling service."
        systemctl daemon-reload
        systemctl enable ipf-teams-webhook.service
        echo
        echo "Starting service."
        systemctl start ipf-teams-webhook.service
        sleep 5
        echo "Printing service status."
        systemctl show -p ActiveState ipf-teams-webhook
        exit 0
      fi

    else
      echo "[Required] Please edit .env to setup to configure connection information."
      echo "IPF_SECRET is provided when configuring the webhook under Settings in the GUI.  Use URL 'http://127.0.0.1:8000/ipfabric'."
      echo "IPF_TOKEN is the token.  Please see README.md for the endpoints it requires.  Only requires RO access."
      echo "TEAMS_URL is provided when configuring the webhook in Microsoft Teams."
      echo ""
      echo "nano /home/integrations/ipfabric-teams-webhook/.env"
      exit 1

    fi
    exit 0

fi

echo
if id -u integrations >/dev/null 2>&1; then
  echo "User integrations exists"
else
  echo "Adding user integrations"
  useradd -m -d /home/integrations -s /bin/bash -U integrations
fi

sudo runuser - integrations -c "pip --version"
if [ $? -eq 0 ]; then
  echo "pip installed"
else
  echo "Installing pip"
  sudo runuser - integrations -c "wget https://bootstrap.pypa.io/get-pip.py -O /home/integrations/get-pip.py"
  sudo runuser - integrations -c "python3 /home/integrations/get-pip.py"
  rm /home/integrations/get-pip.py
fi

echo
sudo runuser - integrations -c "poetry --version"
if [ $? -eq 0 ]; then
  echo "Poetry installed"
else
  echo "Installing poetry"
  sudo runuser - integrations -c "pip install poetry"
  sudo runuser - integrations -c "poetry config virtualenvs.in-project true"
fi

echo
if [ -f /home/integrations/ipfabric-teams-webhook/pyproject.toml ]; then
    echo "Project installed."
else
  echo "Cloning ipfabric-teams-webhook"
  sudo runuser - integrations -c "git clone https://gitlab.com/ip-fabric/integrations/scripts/ipfabric-teams-webhook.git /home/integrations/ipfabric-teams-webhook"
  echo "Installing python packages"
  sudo runuser - integrations -c "cd /home/integrations/ipfabric-teams-webhook && poetry install"
  echo "Copying sample_ipf_alerts.json to ipf_alerts.json"
  sudo runuser - integrations -c "cp /home/integrations/ipfabric-teams-webhook/sample_ipf_alerts.json /home/integrations/ipfabric-teams-webhook/ipf_alerts.json"
  echo "Copying sample.env to .env"
  sudo runuser - integrations -c "cp /home/integrations/ipfabric-teams-webhook/sample.env /home/integrations/ipfabric-teams-webhook/.env"
  echo "Finished Installing."
  echo ""
  echo "[Optional] Please edit ipf_alerts.json to change the alerts you would like to receive notifications for:"
  echo "\`nano /home/integrations/ipfabric-teams-webhook/ipf_alerts.json\`"
  echo ""
  echo "[Required] Please edit .env to setup to configure connection information."
  echo "IPF_SECRET is provided when configuring the webhook under Settings in the GUI.  Use URL 'http://127.0.0.1:8000/ipfabric'."
  echo "IPF_TOKEN is the token.  Please see README.md for the endpoints it requires.  Only requires RO access."
  echo "TEAMS_URL is provided when configuring the webhook in Microsoft Teams."
  echo ""
  echo "\`nano /home/integrations/ipfabric-teams-webhook/.env\`"

fi

echo "Once .env file is configured run install.sh again to test connection and start service."
echo "\`/home/integrations/ipfabric-teams-webhook/install.sh\`"
