#!/home/integrations/ipfabric-teams-webhook/.venv/bin/python
import os
from dotenv import load_dotenv
from ipfabric import IPFClient


if __name__ == '__main__':
    if os.path.isfile('/home/integrations/ipfabric-teams-webhook/.env'):
        load_dotenv('/home/integrations/ipfabric-teams-webhook/.env')

    try:
        ipf = IPFClient()
        print("Connection Successful.")
    except Exception as err:
        print(err)
        print("\nFailed to load IPFClient.  Please check `/home/integrations/ipfabric-teams-webhook/.env`\n")
        exit(1)

    try:
        u = ipf.get('/users')
        print("User Query Successful.")
    except Exception as err:
        print(err)
        print("\nFailed to get list of Users. Integration will run but will not be able to translate userId to username.")
        print("Please ensure IPF_TOKEN has GET access to `/users` to resolve this.\n")
        exit(0)
