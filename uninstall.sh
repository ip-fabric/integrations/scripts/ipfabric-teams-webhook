#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "Stopping service"
systemctl stop ipf-teams-webhook.service
echo "Disabling service"
systemctl disable ipf-teams-webhook.service
echo "Deleting service"
rm -f /etc/systemd/system/ipf-teams-webhook.service
systemctl daemon-reload
